from django import forms


class LogInForm(forms.Form):
    username = forms.CharField(max_length=150)
    password = forms.CharField(
        max_length=150,
        widget=forms.PasswordInput,
    )


class UserSignUpForm(forms.Form):
    username = forms.CharField(max_length=150, widget=forms.TextInput(attrs={
        'placeholder': 'Username',
        'style': 'mx-auto;',
        'class': 'form-control'
        }))
    password = forms.CharField(
        max_length=150,
        widget=forms.PasswordInput(attrs={
            'placeholder': 'Password',
            'style': 'mx-5;',
            'class': 'form-control'
        })
    )
    password_confirmation = forms.CharField(
        max_length=150,
        widget=forms.PasswordInput(attrs={
            'placeholder': 'Confirm Password',
            'style': 'mx-auto;',
            'class': 'form-control'
        })
    )
